const mongoose = require('mongoose');
// mongodb://arvind:123@ds163020.mlab.com:63020/relevo
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/relevo', {
  useMongoClient: true,
});

module.exports = {
  mongoose
};
